#!/usr/bin/env bash

HUGO_PLATFORM=linux_amd64
HUGO_VERSION=0.12

function conditional_download() {
  DOWNLOAD_URL="$1"
  DESTINATION="$2"

  if [ ! -d ${DESTINATION} ]; then
    rm -rf ${DESTINATION}
    mkdir -p ${DESTINATION}
    echo "Downloading ${DOWNLOAD_URL}"
    curl ${DOWNLOAD_URL} -sL | tar xz -C ${DESTINATION}
  fi
}

function install_theme() {
    mkdir themes
    cd themes
    git clone https://github.com/zyro/hyde-x
    cd hyde-x
    git checkout #8c49938
    cd ..
    cd ..
}

HUGO_RELEASE_URI=https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_${HUGO_PLATFORM}.tar.gz

conditional_download ${HUGO_RELEASE_URI} hugo_${HUGO_VERSION}

ln hugo_${HUGO_VERSION}/hugo_${HUGO_VERSION}_${HUGO_PLATFORM}/hugo_${HUGO_VERSION}_${HUGO_PLATFORM} hugo

install_theme