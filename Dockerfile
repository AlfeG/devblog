FROM nginx

RUN apt-get update && apt-get install curl git -y

COPY ./.install/install.sh /app/.install/install.sh

WORKDIR /app

RUN /bin/bash -c "source /app/.install/install.sh"

COPY . /app

RUN ./hugo -t "hyde-x" -d "www"

RUN cp -r /app/www/* /usr/share/nginx/html/