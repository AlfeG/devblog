+++
date = "2015-01-23T15:36:38+02:00"
draft = false
title = "Quickstart with Asp.Net 5 on Dokku"
techs = ["Docker", "Dokku", "Dokku-alt", ".Net"]
aliases = ["/blog/2015/01/22/Dokku-and-aspnet-vNext","/blog/2015/01/22/Dokku-and-aspnet-vNext/"]
+++

# story

Here is a quickstart tutorial, on how to deploy ASP.NET 5 web application on docker with help of dokku

## what I will use

* Ubuntu box 
    * I've used [vultr](http://www.vultr.com/?ref=6808795) server
    * You can use [DigitalOcean](https://www.digitalocean.com) or any other cloud/virtual machine
* [dokku-alt](https://github.com/dokku-alt/dokku-alt)
* [microsoft/aspnet](https://registry.hub.docker.com/u/microsoft/aspnet/) Docker image
* my windows pc with toolbelt that include putty apps - putty, puttygen, plink, pageant
    * http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html


## dokku

[Dokku](https://github.com/progrium/dokku) is Docker powered mini-Heroku. "The smallest PaaS implementation you've ever seen" as stated on their page. 
[Dokku-alt](https://github.com/dokku-alt/dokku-alt) is powerfull fork, that support a lot of nice things out of the box. One of those cool features is ability to deploy application from Dockerfile.

# getting started

First of all we need some server to play with. I recommend to use clean ubuntu server install. 

Also we need some [hostname](https://www.google.com/search?q=register+free+domain) for server. I will use `server.alfeg.net`

I will suppose that at this point reader has clean install with only root login and password for newly created Ubuntu 14 box.

## those pesky keys

We need to generate two SSH keys, one for root login, other for dokku user.

There is a bunch of articles that can help with keys generation:

* http://www.rackspace.com/knowledge_center/article/generating-rsa-keys-with-ssh-puttygen
* http://katsande.com/using-puttygen-to-generate-ssh-private-public-keys

If you don't want to read, then just use puttygen it's pretty simple.

![PuttyGen Ui with key generated](/img/PuTTYKEyGenerator2.png)

You need to save privatekey, and "Public key for pasting into OpenSSH authorized_keys file"
 as `login.ppk` and `login.pub` accordingly. Press generate again and generate `dokku.ppk` and `dokku.pub`. 
 Please do not forget that private keys should never be send over the wire.
 
Launch `pageant.exe` and using `Add key` menu add two ppk keys. This will allow us to SSH without thinking of logins and passwords. SSH magic will happen.

## make ssh magic happen

Use putty to SSH to Ubuntu box. From command line it's easy as

    putty -ssh -l root server.alfeg.net
    
`root` in command above is the name of user with root rights. At this point SSH will ask for root password, I hope You have it. It's last time You use it.

In home directory of Ubuntu user there is an `.ssh` folder with `authorized_keys` file inside. 
Each line of this file represent authorized key that can be used for login. So we need to add content of `login.pub` to this file(I use sample value):

    echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAr1UIsxCPLIVs1Kn49fBuuDjUamZIOGaY3WPVaU9PeTNwrfs328oKgKgOI/lsyCwtxrWX02X0orvpKM2zMi3U1vzMNSBCZztk8XWqCs8kGPEhDgi/Tyi55CNhipWQ+UYj/hAmVaKWdUU4Q8RXkozmlHXczd193QUlrhNf59Nzq2zMZ3Q1Ng10lMNJtkCjQ0/8MW7qo/OzRe5QI6vWJykCvrbn+QPk2pnT4tTWazYJ9JE1Nr5kbyuF2H0jpWpUe9lMaL92JqAd/abDVjPQ1XObiK4N92ZiXwVWvIh5vhlfRPH99nld+Vg+7YV455SThXOfDv2TKeAFYIIIyjNx8wY1Rw== rsa-key-20150121" >> ~/.ssh/authorized_keys
    
`rsa-key-20150121` is just a comment. There can be any symbols. I write there name of user login.

At this moment You can login withou password. Pageant will do da magic.

## installing dokku-alt

Let's go to real things. And some caveats for windows users. 

Let's install dokku-alt at first. Please note that some thing's can change over time. Refer to project wiki: https://github.com/dokku-alt/dokku-alt#installing

     sudo bash -c "$(curl -fsSL https://raw.githubusercontent.com/dokku-alt/dokku-alt/master/bootstrap.sh)"
     
At some time installing will stop waiting for user input. What dokku-alt is expecting is that user navigate it's browser to `http://localhost:2000`, thing that cannot be done via SSH. Putty to the rescue. We will forward remote 2000 port to local PC

![Putty magic, port forwarding](/img/2015-01-21_21-25-27.png)

Navigate on local PC to http://localhost:2000, there will be `dokku` install page. Fill HOST field with Your host, fill public key from `dokku.pub`, choose how new applications will be served, as subdomains or as folders.

After pressing submit, open putty with SSH session and press CTRL+C to abort installer. At this point we have 'dokku-alt' and docker fully installed. Dokku user public key is saved in `/home/dokku/.ssh/authorized_keys`

## aspnet vNext fun

Read this wonderfull article that introduce microsoft/aspnet docker image: http://blogs.msdn.com/b/webdev/archive/2015/01/14/running-asp-net-5-applications-in-linux-containers-with-docker.aspx

For our PaaS we need only Step 2. Server we already prepared, dokku-alt will handle docker magic. 
Commit changes. Make sure that Dockerfile is in root. Here is a text for Dockerfile

    FROM microsoft/aspnet

    COPY project.json /app/

    WORKDIR /app

    RUN ["kpm", "restore"]

    COPY . /app

    EXPOSE 5004

    ENTRYPOINT ["k", "kestrel"]

Commit all files

    git add . && git commit -m "Adding fun"

Add git remote for our new handmade cloud server.

    $ git remote add dokku dokku@server.alfeg.net:helloAsp
    $ git push dokku master
    
Remote will show deployment log, and report that everything is OK and server is available at some URL. 
BUT at the moment of this post creation application is not working. 
Reason is this `-t` argument in docker that need to send for `Kestrel` server ability to work.

> The -t switch attaches a pseudo-tty to the container (this switch will not be necessary in future versions of ASP.NET 5).
    
By default dokku-alt is not adding this switch to docker. 
To make this happen we need to go to our SSH session and create a file inside of our newly created application folder. 
Application folder is located in `dokku` user home folder, in my case it's

    cd /home/dokku/helloAsp
    
Create here a DOCKER_ARGS file with one simple content `-t`

    echo "-t" > DOCKER_ARGS
    
Now restart application

    dokku stop helloAsp
    dokku start helloAsp
    
Tada: Open browser - you application is alive and kicking. In my case this were http://helloAsp.ocean.alfeg.net.